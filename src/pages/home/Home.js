import React from "react";
import './Home.css';

import Navbar from "../component/navbar/Navbar";
import Footer from "../component/footer/Footer";

import disk from "../../assets/disk.svg"

import right_arr from "../../assets/right-arrow.svg"

import img1 from "../../assets/img1.png"
import img2 from "../../assets/img2.png"
import img3 from "../../assets/img3.png"
import img4 from "../../assets/img4.png"
import img5 from "../../assets/img5.png"

import imgGrid1 from "../../assets/img-grid1.png"
import imgGrid2 from "../../assets/img-grid2.png"
import imgGrid3 from "../../assets/img-grid3.png"
import imgGrid4 from "../../assets/img-grid4.png"
import imgGrid5 from "../../assets/img-grid5.png"
import imgGrid6 from "../../assets/img-grid6.png"
import imgGrid7 from "../../assets/img-grid7.png"
import imgGrid8 from "../../assets/img-grid8.png"

import friend1 from "../../assets/friend1.png"
import friend2 from "../../assets/friend2.png"
import friend3 from "../../assets/friend3.png"
import friend4 from "../../assets/friend4.png"
import friend5 from "../../assets/friend5.png"
import friend6 from "../../assets/friend6.png"
import friend7 from "../../assets/friend7.png"
import friend8 from "../../assets/friend8.png"

import service1 from "../../assets/service-1.png"
import service2 from "../../assets/service-2.png"
import service3 from "../../assets/service-3.png"
import service4 from "../../assets/service-4.png"



export default class Home extends React.Component{
    render(){
        return(
            <>
                
                <Navbar></Navbar>


                <div className="section home-sec1">
                    <div className="container">
                        <img src={disk} className="home-sec1-img" alt=""/>
                        <h1 className="c-white-1 t-64-77 t-800 t-upper f-syne home-sec1-h">
                            Innovation Studio
                        </h1>
                        <p className="c-white-1 t-24-36 c-grey-1 home-sec1-p">
                            Symba is an innovation studio that specializes not only in getting ideas coded into real world applications but also getting those ideas in front of the right audience. We are here to assist you every step of the way. 
                        </p>
                        <div className="flex-r v-center home-sec1-btn-wrap t-700 c-white-1 t-upper">
                            <a href="" className="flex-r v-center h-center t-16-24 t-spacing-20">
                                Get Started
                            </a>
                            <a href="" className="flex-r v-center h-center t-18-27 t-spacing-30">
                                Explore
                            </a>
                        </div>
                    </div>
                </div>


                <div className="section home-sec2">
                    <div className="container home-sec2-c c-white-1">
                        <div className="flex-c home-sec2-itm">
                            <img src={img1} alt=""/>
                            <h2 className="f-syne t-600 t-36-43 line-clamp clamp-1">
                                Symba Records
                            </h2>
                            <p className="t-18-27 c-grey-1 no-margin">
                                We are a r specializing in press placements, brand voice, and storytelling
                            </p>
                        </div>
                        <div className="flex-c home-sec2-itm">
                            <img src={img2} alt=""/>
                            <h2 className="f-syne t-600 t-36-43 line-clamp clamp-1">
                                Symba Publishing
                            </h2>
                            <p className="t-18-27 c-grey-1 no-margin">
                                We are a r specializing in press placements, brand voice, and storytelling
                            </p>
                        </div>
                        <div className="flex-c home-sec2-itm">
                            <img src={img3} alt=""/>
                            <h2 className="f-syne t-600 t-36-43 line-clamp clamp-1">
                                7feur.com
                            </h2>
                            <p className="t-18-27 c-grey-1 no-margin">
                                We are a r specializing in press placements, brand voice, and storytelling
                            </p>
                        </div>
                    </div>
                </div>


                <div className="section home-sec3">
                    <div className="container home-sec3-c">
                        <div className="left flex-r h-left">
                            <img alt="" src={img4}/>
                            <img alt="" src={img5}/>
                        </div>
                        <div className="right flex-c h-left c-white-1">
                            <h2 className="t-24-28 t-800 f-syne no-margin">
                                Our thesis is centered around, but not limited to, helping companies in the music and web3 space
                            </h2>
                            <a href="" alt="" className="home-sec3-btn flex-r v-center h-center t-18-27 t-700 t-spacing-30">
                                Explore
                            </a>
                        </div>
                    </div>
                </div>


                <div className="section home-sec4 c-white-1">
                    <div className="container flex-c h-center">
                        <h2 className="home-sec4-h t-center f-syne t-800 t-48-57">
                            The services we offer are:
                        </h2>
                        <div className="home-sec4-grid">

                            <a href="" className="itm-top flex-r v-center">
                                <img src={imgGrid1} alt=""/>
                                <div className="inner-wrap flex-c">
                                    <div className="t-700 f-syne t-16-19 line-clamp clamp-2">
                                        Design
                                    </div>
                                    <div className="flex-r just-space t-14-21 v-center c-purple-1 btm-txt">
                                        See our work <img src={right_arr} alt=""/>
                                    </div>
                                </div>
                            </a>
                            <a href="" className="itm-top flex-r v-center">
                                <img src={imgGrid2} alt=""/>
                                <div className="inner-wrap flex-c">
                                    <div className="t-700 f-syne t-16-19 line-clamp clamp-2">
                                        Solidity Dev & Dev Ops 
                                    </div>
                                    <div className="flex-r just-space t-14-21 v-center c-purple-1 btm-txt">
                                        See our work <img src={right_arr} alt=""/>
                                    </div>
                                </div>
                            </a>
                            <a href="" className="itm-top flex-r v-center">
                                <img src={imgGrid3} alt=""/>
                                <div className="inner-wrap flex-c">
                                    <div className="t-700 f-syne t-16-19 line-clamp clamp-2">
                                        PR
                                    </div>
                                    <div className="flex-r just-space t-14-21 v-center c-purple-1 btm-txt">
                                        See our work <img src={right_arr} alt=""/>
                                    </div>
                                </div>
                            </a>
                            <a href="" className="itm-top flex-r v-center">
                                <img src={imgGrid4} alt=""/>
                                <div className="inner-wrap flex-c">
                                    <div className="t-700 f-syne t-16-19 line-clamp clamp-2">
                                        Funding
                                    </div>
                                    <div className="flex-r just-space t-14-21 v-center c-purple-1 btm-txt">
                                        See our work <img src={right_arr} alt=""/>
                                    </div>
                                </div>
                            </a>



                            <a href="" className="itm-btm flex-c v-top h-left">
                                <img src={service1} alt=""/>
                                <div className="inner-wrap flex-c">
                                    <div className="t-700 f-syne t-16-19 line-clamp clamp-2">
                                        Community building & community management
                                    </div>
                                    <div className="flex-r t-14-21 v-center c-purple-1 btm-txt">
                                        See our work <img src={right_arr} alt=""/>
                                    </div>
                                </div>
                            </a>
                            <a href="" className="itm-btm flex-c v-top h-left">
                                <img src={service2} alt=""/>
                                <div className="inner-wrap flex-c">
                                    <div className="t-700 f-syne t-16-19 line-clamp clamp-2">
                                        Podcast Creation/ Management/ Promotion
                                    </div>
                                    <div className="flex-r t-14-21 v-center c-purple-1 btm-txt">
                                        See our work <img src={right_arr} alt=""/>
                                    </div>
                                </div>
                            </a>
                            <a href="" className="itm-btm flex-c v-top h-left">
                                <img src={service3} alt=""/>
                                <div className="inner-wrap flex-c">
                                    <div className="t-700 f-syne t-16-19 line-clamp clamp-2">
                                        Paid ads: Google, Facebook, Instagram
                                    </div>
                                    <div className="flex-r t-14-21 v-center c-purple-1 btm-txt">
                                        See our work <img src={right_arr} alt=""/>
                                    </div>
                                </div>
                            </a>
                            <a href="" className="itm-btm flex-c v-top h-left">
                                <img src={service4} alt=""/>
                                <div className="inner-wrap flex-c">
                                    <div className="t-700 f-syne t-16-19 line-clamp clamp-2">
                                        Influencer Campaigns 
                                    </div>
                                    <div className="flex-r t-14-21 v-center c-purple-1 btm-txt">
                                        See our work <img src={right_arr} alt=""/>
                                    </div>
                                </div>
                            </a>


                        </div>
                    </div>
                </div>


                <div className="section home-sec5 c-white-1">
                    <div className="container flex-c h-center">
                        <h2 className="home-sec5-h f-syne t-800 t-48-57 t-upper">
                            FRIENDS
                        </h2>
                        <div className="home-sec5-img-wrap">
                            <img src={friend1} alt=""/>
                            <img src={friend2} alt=""/>
                            <img src={friend3} alt=""/>
                            <img src={friend4} alt=""/>
                            <img src={friend5} alt=""/>
                            <img src={friend6} alt=""/>
                            <img src={friend7} alt=""/>
                            <img src={friend8} alt=""/>
                        </div>
                        <p className="t-center no-margin t-18-27 c-grey-1 home-sec5-p">
                            Design / Solidity Dev & Dev Ops / PR  / Community building & community management - Discord  - Telegram -Social media / Podcast Creation / Management / Promotion / Paid ads: Google, Facebook, Instagram / Influencer Campaigns / Billboard Advertising / Funding
                        </p>
                    </div>
                </div>


                <div className="section home-sec6 c-white-1">
                    <div className="container home-sec6-c">
                        <div className="home-sec6-wrap flex-c h-center">
                            <h2 className="home-sec6-h t-center f-syne t-800 t-36-62">
                                We take a % of every company we work with, and also require an up-front retainer. 
                            </h2>
                            <a href="" className="home-sec6-btn t-18-27 t-700 t-spacing-30 t-upper">
                                Register now
                            </a>
                        </div>
                    </div>
                </div>


                <Footer></Footer>

                
            </>
        )
    }
}