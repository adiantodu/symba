
import './Footer.css';

import logo_ico from "../../../assets/logo_ico.svg"

import fb from "../../../assets/fb-1.svg"
import ig from "../../../assets/ig-1.svg"
import link from "../../../assets/link-1.svg"
import dc from "../../../assets/dc-1.svg"
import twt from "../../../assets/twt-1.svg"
import yt from "../../../assets/yt-1.svg"
import tele from "../../../assets/tele-1.svg"

function Footer() {
    return (
        <div className="section footer c-white-1">
            <div className='container'>
                <img src={logo_ico} alt="" className='footer-logo'/>
            </div>
            <div className="container flex-r just-space">
                <div className="footer-left">
                    <p className="t-14-21 t-spacing-3 footer-p">
                        We are a public relations agency specializing in press placements, brand voice, and storytelling
                    </p>
                    <div className="footer-social-wrap flex-r v-center">
                        <a href="">
                            <img src={yt} alt=""/>
                        </a>
                        <a href="">
                            <img src={link} alt=""/>
                        </a>
                        <a href="">
                            <img src={tele} alt=""/>
                        </a>
                        <a href="">
                            <img src={ig} alt=""/>
                        </a>
                        <a href="">
                            <img src={twt} alt=""/>
                        </a>
                        <a href="">
                            <img src={dc} alt=""/>
                        </a>
                        <a href="">
                            <img src={fb} alt=""/>
                        </a>
                    </div>
                </div>
                <div className='footer-menu flex-c h-left t-14-21 t-spacing-3'>
                    <a href="">
                        Home
                    </a>
                    <a href="">
                        Index
                    </a>
                    <a href="">
                        FAQs
                    </a>
                    <a href="">
                        Contact
                    </a>
                </div>
                <div className='footer-menu flex-c h-left t-14-21 t-spacing-3'>
                    <a href="">
                        Terms of services
                    </a>
                    <a href="">
                        Privacy policy
                    </a>
                    <a href="">
                        Cookie policy
                    </a>
                </div>


                <div className='footer-form'>
                    <div className='t-14-21 t-spacing-11 t-700'>
                        Sign up for our latest updates
                    </div>
                    <div className='footer-input'>
                        <input type="email" className='t-14-21' placeholder="Enter your email address..." />
                        <button className='t-14-21 t-700 t-spacing-10 cursor'>
                            sign up
                        </button>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Footer;



