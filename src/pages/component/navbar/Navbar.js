// import { NavLink, Link } from "react-router-dom";
import './Navbar.css';
import logo from "../../../assets/logo.svg"

function Navbar() {
    return (
        <div className="navbar section">
            <div className="container flex-r v-center just-space">
                <a href="" alt="">
                    <img src={logo} alt="logo" className='nav-logo'/>
                </a>


                <div className="nav-menu">
                    <a href="" className="t-600 t-18-27 c-white-1">
                        Home
                    </a>
                    <a href="" className="t-600 t-18-27 c-white-1">
                        Records
                    </a>
                    <a href="" className="t-600 t-18-27 c-white-1">
                        Books
                    </a>
                    <a href="" className="t-600 t-18-27 c-white-1">
                        Ventures
                    </a>
                </div>
            </div>
        </div>
    );
}

export default Navbar;



